#TgCache 天狗文件缓存

缓存是解决应用性能的一直方式，JAVA也自带了缓存JSR-107标准，同时也有常用的JAVA缓存有很多如Ehcache、OSCache、Apache Commons JCS ……很多缓存架构（也支持基础）。
TgCache是一个JAVA缓存工具，出发点只是想做一个简单的文件缓存，用于JAVA客户端AWT、SWING，JAVAFX一些客户端缓存。由于在常见的JAVA缓存中大多数是内存缓存，而TgCache就是一个文件缓存。

![流程图](https://git.oschina.net/uploads/images/2017/0523/182133_e205017e_71.png "在这里输入图片标题")
```
CacheEngine engine = new TgCacheEngine();//创建缓存    
engine.add("name",  "tngou");//存在  默认的储存库为 default
engine.add("table", "time", new Date()); //缓存库 table ,key=tile,value= 时间对象    
CacheEngine engine1 = new TgCacheEngine();
Object name = engine1.get("name"); //取值
Object time = engine1.get("table", "time"); //取值
System.err.println(name+":"+time);
engine.clear("table");//清除缓存
engine.stop(); //关闭
```


#如何使用
下载[TgCache.x.x.jar](http://git.oschina.net/397713572/tgcache/attach_files) 
下载TgCache依赖库 [apache commons-io](http://commons.apache.org/proper/commons-io/)

#引入项目
分别把TgCache.x.x.jar、commons-io-x.x.jar 导入到项目中。
注意：TgCache.x.x.jar是基于JDK8编译的，如果需要低版本的，可以自己[下载源码](http://git.oschina.net/397713572/tgcache)编译。

#使用文档

[TgChache中文文档](http://git.oschina.net/397713572/tgcache/wikis)

